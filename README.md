# WhosWho

Create PDF documents with people pictures and corresponding name, also called
"who's who", "yearbook" or "facebook".

From a set of pictures and names, WhosWho creates a PDF document with faces
and corresponding names, as used in schools. WhosWho tries to make things
as simple as possible for the user:


- Pictures can be automatically cropped to keep only the person's face.
- If you have a ODS/XLSX file containing people names, just export it to CSV
  and WhosWho will be able to use it.
- A default picture can be used if you do not have a person's picture.
- Multiple sheet layouts are available, each one in 150 and 300 DPI:
  - A4, portrait, 4x5
  - A4, portrait, 5x6
  - A4, portrait, 6x6
  - A4, landscape, 6x4
  - A4, landscape, 7x4
  - A3, portrait, 6x8
  - A3, landscape, 9x6

If you find this software useful, please leave me an email to let me know!


# Screenshots

![WhosWho screenshot](https://framagit.org/Yvan-Masson/WhosWho/-/raw/master/screenshot.png "WhosWho screenshot")


For an example, have a look at the following document
([PDF version](https://framagit.org/Yvan-Masson/WhosWho/-/blob/master/example%20output.pdf)),
generated from pictures and CSV file available in the
[example directory](https://framagit.org/Yvan-Masson/WhosWho/-/tree/master/example).
Pictures were automatically cropped around faces, but the program failed for a
few (e.g. Edward Snowden), in which case the original picture was used. Face
detection works best when face is in front of the camera and when there is an
uniform background. You can of course provide another picture if face detection
does not work properly.


![output example](https://framagit.org/Yvan-Masson/WhosWho/-/raw/master/example%20output.png "output example")


# Installation

WhosWho requires ImageMagick, Python 3 and the following Python modules:

- Chardet
- OpenCV
- Pillow
- Willow


## With Flatpak

WhosWho is available
[from Flathub](https://flathub.org/apps/details/fr.masson_informatique.WhosWho),
for easy installation. It will work on Debian ≥ 11 (Bullseye), Ubuntu ≥ 21.04
(Hirsute), Linux Mint ≥ 20.3 (Una), Fedora ≥ 33.

For other Linux distributions, know that it requires `xdg-desktop-portal-gtk`
[≥ 1.7.1](https://docs.flatpak.org/en/latest/portals-gtk.html) if you use Gnome
and derivatives environments, or `xdg-desktop-portal-kde`
[≥5.18.90](https://docs.flatpak.org/en/latest/portals-qt.html) if you use KDE or
LXQT.


## Manually on Debian ≥ 11, Ubuntu ≥ 18.04 and derivatives


You may prefer installing ImageMagick and Python libraries from your
distribution rather than from [PyPI](https://pypi.org/). In that case run:

```
$ sudo apt install imagemagick python3-chardet python3-pil python3-willow
```

You can then download the .whl file from
[latest release](https://framagit.org/Yvan-Masson/WhosWho/-/releases) and use
`pip` to install WhosWho and its remaining dependency:

```
$ pip3 install WhosWho-<version>-py3-none-any.whl
```

Depending of your version of ImageMagick, it is possible that final PDF
conversion fails with error "attempt to perform an operation not allowed by the
security policy \`PDF\`". In this case, please edit as root
`/etc/ImageMagick/policy.xml` and comment the line:

```
<policy domain="coder" rights="none" pattern="PDF" />
```

Like this:

```
<!--<policy domain="coder" rights="none" pattern="PDF" />-->
```

For explanations on this issue, you can read this
[bug report](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=964090#56).


## With Snap

I do not plan to spend time on a Snap package, but any contribution is welcome.


## Windows

WhosWho should run on Windows, but:

- it would need a few small modifications to the code
- I have no time to find how it should be packaged and distributed


# Thanks

WhosWho is available in different languages thanks to:

- AHOHNMYC (Russian)
- Albano Battistella (Italian)
- Allan Nordhøy (Norwegian Bokmål)
- Ettore Atalan (German)
- gallegonovato (Spanish)
- Heimen Stoffels (Dutch)
- Jean-Luc Tibaux (German)
- Milo Ivir (Croatian)
- Nathan Bonnemains (French)
- Petter Reinholdtsen (Norwegian Bokmål)
- Quiwy (Spanish)
- Sabri Ünal (Turkish)
- தமிழ்நேரம்  (Tamil)


# Contributions

Contributions are welcome!

Translators can use
[Hosted Weblate](https://hosted.weblate.org/projects/whoswho/main/), generously
developped and hosted by [Weblate](https://weblate.org).


